## Threat

Threat is the potential bad outcome of your action. The default threat level is *moderate*.

### Low Threat

You're in a controlled position.

### Moderate Threat

You're in a risky position.

### High Threat

You're in a desperate position. Mark xp.

## Effect

Effect is the outcome of your action should you succeed. Most actions will have the *expected* effect, but some things (like kicking a soccer ball into outer space) are impossible. These actions will have *no* effect. A [[boost]] can still raise *no effect* to *limited effect*.

### No Effect

You cannot achieve what you set out to do.

### Limited Effect

You're able to accomplish part of your objective.

### Expected Effect

You achieve what you set out to do.

### Great Effect

You're able to achieve as much as you wanted, and then some.

## Upping the Ante

You can raise threat to also raise effect by the same degree.

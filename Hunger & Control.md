The beast inside you hungers for that sweet essence of life that eludes you in undeath, but so long as your humanity remains intact, you can keep it under control.

You have the capacity for nine marks of *hunger*—the unnatural thirst for blood. You also start with three marks of *control*—how well you can keep the beast within you shackled.

Your control rating is equal to `floor((9 - [hunger]) / 3)`. In other words, for every three marks of hunger, you lose one mark of control; and when you clear hunger, you also regain a mark of control for each group of three hunger cleared. See the following table:

| Hunger | Control |
| ------ | ------- |
| 0      | 3       |
| 1      | 3       |
| 2      | 3       |
| 3      | 2       |
| 4      | 2       |
| 5      | 2       |
| 6      | 1       |
| 7      | 1       |
| 8      | 1       |
| 9      | 0       |

## Recovering from Hunger

There is only one thing that can slake your undead thirst: blood. Not just any blood will do—fresh blood taken from a living human is strong and willful, enough to sustain a vampire for days. You may clear hunger with either the [[Feed]] or the [[Feast]] moves.

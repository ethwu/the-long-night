When you roll a rating, you assemble a dice pool with a number of six-sided dice equal to the rating's value. The *result* of the roll is the highest individual value rolled. If the value of the rating is zero, then roll two dice. The result will be the lower of the two values rolled.

In addition to the result, rolls also have a *degree of success*:

- If the result was 1–3, the degree of success is a **miss**.
- If the result was 4–5, the degree of success is an **imperfect hit**.
- If the result was 6, the degree of success is a **perfect hit**.

Furthermore, if two or more 6s were rolled, the roll is a **critical**. **Critical** rolls usually embody some additional benefit. A **critical** is always a perfect hit.

## Boosting Rolls

Before a roll, you may add one *boost* to your roll. There are several ways to get boosts. A boost adds one die to the roll, increases effect, or allows you to act while incapacitated.

### Push Yourself

You may take 2 hunger to *push yourself* to boost a roll.

### Devil's Bargain

Instead of pushing yourself, the GM or another player may offer a *devil's bargain*. If you accept it, you may add a boost to your roll. The devil's bargin takes place regardless of whether you succeed at an action. Devil's bargains are always optional. Example devil's bargains include:

- Collateral damage or unintended harm.
- Betray a friend or loved one.
- Offend or anger a loved one.
- Start and/or tick a troublesome clock.
- Suffer [[heat]].
- Suffer harm.

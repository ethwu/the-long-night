The life of the dead brings you to your limits. When you _face a grievous injury_ or *experience something that challenges your humanity*, you take *shock* to the appropriate virtue.

While a virtue is *shocked*, replace one die of every action taken with a *terror die*, as described in [[Take Action]].
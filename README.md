# The Long Night

This is a work in progress. You will need a copy of *Blades in the Dark* as well as *Vampire: The Masquerade 20th Anniversary Edition*.

## Attribution

This work is based on *Blades in the Dark* (found at http://www.bladesinthedark.com/), product of One Seven Design, developed and authored by John Harper, and licensed for our use under the Creative Commons Attribution 3.0 Unported license (http://creativecommons.org/licenses/by/3.0/).

### Inspiration

- *Blades in the Dark*, by John Harper
- *Vampire: The Masquerade 20th Anniversary Edition*, published by White Wolf Publishing
- *Undying*, by Paul Riddle
- *Girl by Moonlight*, by Andrew Gillis
- *CBR+PNK*, by Emanoel Melo
- *Wicked Ones*, by Ben Nielson

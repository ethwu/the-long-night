When you *share a moment or have an argument* with a compatriot, you each gain a string on each other.

> [!info] Strings
> You can spend a string you have on another compatriot to:
> - Force them to protect you.
> - Grant them a boost.

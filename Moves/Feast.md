When you *feast on a living victim*, take one of the following:

- You leave your victim alive, intoxicated in a state of ecstacy. Clear 1 hunger.
- You leave your victim on the brink, drained but alive. If they do not receive medical attention soon, they will die. Clear 2 hunger.
- You savor every last drop, killing them instantly. Clear 3 hunger.

and roll your weakest virtue:

- **Perfect**: Take 2 edges.
- **Imperfect**: Take 1 edge, or 2 edges and 1 flaw.
- **Miss**: Take 1 flaw, or 1 edge and 2 flaws.

## Edges

- You close the wound, leaving no trace.
- You cloud your victim's mind, causing them to forget the encounter entirely.
- You sweep in and out like a shadow—your attack evades notice completely.

## Flaws

- It's messy—someone will find out.
- You're left wanting—clear one less hunger.
- They put up a fight. You'll need a minute to recover before your strength returns to full.
- It isn't enough—you are consumed with a desire for *more*.

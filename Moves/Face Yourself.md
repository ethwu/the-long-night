When you *question how much humanity you have left*, roll `[humanity]`:

- **Perfect**: You're still in charge, right?
- **Imperfect**: You are on the edge.
- **Miss**: How much of you is there left? Lose one humanity. If you have no humanity left, you are lost.
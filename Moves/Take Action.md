When you **take matters into your own hands under threat**, the GM establishes the potential [[threat and effect]]. Take a boost by *pushing yourself* or by accepting a *devil's bargain*, and roll the appropriate action rating:

- **Perfect**: You do it. The *effect* takes place.
- **Imperfect**: You do it—the *effect* comes to be, but with a *complication*. 
- **Miss**: You don't do it—instead, the established *threat* takes hold. Alternatively, you *do* do it—but at a *dire cost*.

If the action rating chosen falls under a *shocked* virtue, replace one of the dice with a *terror die*. Resolve the roll as normal, but with the following additional conditions based on the degree of the terror die:

- **Perfect**: You charge ahead, more confident than ever. Clear the shock.
- **Imperfect**: You're able to keep going, but for how long? Take one:
	- A compatriot spends a string to help you through this, or you force them to.
	- Take 2 hunger.
	- Attempt to [[Stay in Control]].
- **Miss**: Something fails. Attempt to [[Stay in Control]].
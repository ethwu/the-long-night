At any moment, you may *flash back* to any moment in your past. Anything you do during a flashback may not contradict established fiction, but otherwise is unrestricted. The GM will determine an appropriate cost—typically hunger—but the flashback does not take place if you do not accept the cost.

Typical costs include:

- Something entirely in-character and expected would cost nothing.
- Something unusual, but definitely possible, would cost 1 hunger.
- Something outlandish would cost 2 hunger.

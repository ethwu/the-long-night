Sometimes, something happens that you couldn't have prepared for—or maybe you have prepared for it.

When you *suffer a negative consequence of your or someone else's actions*, you may roll an appropriate virtue (or CONTROL) to reduce or negate the consequence (at the GM's discretion). Take hunger equal to 6 minus the result. If the virtue is *shocked*, take hunger equal to 5 minus the result.
When you *struggle to keep control of your inner beast*, roll `[control]`:

- **Perfect**: You recenter yourself and focus on your humanity.
- **Imperfect**: You struggle to stay in control of yourself. If you accept their help, a fellow compatriot can spend a string to keep you on the right side.
- **Miss**: You fall into *frenzy*.
